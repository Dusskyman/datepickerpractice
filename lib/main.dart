import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DatePickerPractice(),
    );
  }
}

class DatePickerPractice extends StatefulWidget {
  @override
  _DatePickerPracticeState createState() => _DatePickerPracticeState();
}

class _DatePickerPracticeState extends State<DatePickerPractice> {
  List<String> path = [
    'https://cdn.wallpapersafari.com/60/91/5V8p6f.jpg',
    'https://i.pinimg.com/originals/7f/a6/76/7fa67607339f376a6171272cb5bd12e4.jpg',
    'https://wallpaperaccess.com/full/810821.jpg',
    'http://avante.biz/wp-content/uploads/Anime-wallpaper-for-phone/Anime-wallpaper-for-phone-007.jpg',
    'https://preview.redd.it/h0powyt5cy451.png?width=2223&format=png&auto=webp&s=334a3643ec59adee1620e458d754f22fa242f6a2',
    'https://www.xtrafondos.com/en/descargar.php?id=3717&resolucion=2560x1440',
    'https://www.wallpapertip.com/wmimgs/176-1763005_autumn-wallpaper-anime.jpg',
    'https://mocah.org/uploads/posts/334817-Sky-Lantern-Scenery-Anime-4K.jpg',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRVBF2TG4Q7jq4VxRWBU3bCjXDe1HyXNQoDnQ&usqp=CAU',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ5yG7DB5kOu7dhBhaiPALuAYPUwGKjSGR9Fw&usqp=CAU',
    'http://avante.biz/wp-content/uploads/Phone-Anime-wallpapers-HD/Phone-Anime-wallpapers-HD1.jpg',
    'https://cutewallpaper.org/21/anime-phone-wallpaper-hd/Anime-Stars-Mobile-Wallpaper-HD-MobileWallpaper.jpg',
  ];
  DateFormat dateFormat = DateFormat().add_yMd();
  String dateFormated = DateFormat().add_yMd().format(DateTime.now());

  DateTime dateTime = DateTime.now();

  _selectDate(BuildContext context) async {
    final DateTime date = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2022),
    );
    if (date != null && date != DateTime.now()) {
      setState(() {
        dateTime = date;
        dateFormated = dateFormat.format(date);
        print(dateTime.month.toString());
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          color: Colors.grey,
          padding: EdgeInsets.only(top: 30),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ColoredBox(
                color: Colors.grey[300],
                child: Text(
                  dateFormated,
                  style: TextStyle(fontSize: 25),
                ),
              ),
              RaisedButton(
                child: Text('Изменить дату'),
                onPressed: () => _selectDate(context),
              )
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.85,
          child: Image.network(
            path[dateTime.month - 1],
            fit: BoxFit.fill,
          ),
        ),
      ],
    ));
  }
}
